from pydantic import BaseSettings
from sqlalchemy.ext.declarative import declarative_base


class Settings(BaseSettings):
    API_V1_STR: str = '/api/v1'
    DB_URL: str = "postgres+asyncpg://api_curso_ns4h_user:UA3bS6mmLT9O1y0WZCGAxgYqlWnytgyA@postgres.render.com:5432/api_curso_ns4h"
    DBBaseModel = declarative_base()

    class Config:
        case_sensitive = True


settings = Settings()
